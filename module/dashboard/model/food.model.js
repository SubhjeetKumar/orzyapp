const mongoose = require('mongoose');
const User = require('../../signup/model/signup.model');

const foodSchema = new mongoose.Schema({
    user_id: {
        type: mongoose.ObjectId,
        ref: User
    },
    food_item: {
        type: String
    },
    category: {
        type: String
    },
    serving: {
        type: String
    },
    weight: {
        type: String
    },
    protein: {
        type: String
    },
    carbs: {
        type: String
    },
    fat: {
        type: String
    },
    total_calories: {
        type: String
    },
    created_at: {
        type: Date,
        default: Date.now
    }
})

const foodModel = new mongoose.model("foods", foodSchema);
module.exports = foodModel;