const mongoose = require('mongoose');
const User = require('../../signup/model/signup.model')

const heartrateSchema = mongoose.Schema({
user_id:{
    type:mongoose.ObjectId,
    ref: User
},
createddate:{ 
    type:Date,
    default:Date.now()
},
currentheartrate:{
   type:String
},
deleted:{
    type: String, enum: ['yes', 'no']
}
});

module.exports = mongoose.model('Heartrate', heartrateSchema);