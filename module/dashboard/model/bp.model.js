


const mongoose = require('mongoose');
const User = require('../../signup/model/signup.model')

const bpSchema = mongoose.Schema({
user_id:{
    type:mongoose.ObjectId,
    ref: User
},
createddate:{ 
    type:Date,
    default:Date.now()
},
currentBP_DBP:{
   type:String
},
currentBP_SBP:{
  type:String   
},
deleted:{
    type: String, 
    enum: ['yes', 'no'],
    default:'no'
}
});

module.exports = mongoose.model('Bp', bpSchema);