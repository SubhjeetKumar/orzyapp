const mongoose = require('mongoose');
const User = require('../../signup/model/signup.model')

const updatedsleepSchema = mongoose.Schema({
user_id:{
    type:mongoose.ObjectId,
    ref: User
},
createddate:{ 
    type:Date,
    default:Date.now()
},
sleep:{
   type:Number
},
unit:{
    type:String,
    enum: ['hr', 'mm']   
}
});

module.exports = mongoose.model('Updatedsleep', updatedsleepSchema);