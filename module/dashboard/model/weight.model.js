const mongoose = require('mongoose');
const User = require('../../signup/model/signup.model')

const weightSchema = mongoose.Schema({
user_id:{
    type:mongoose.ObjectId,
    ref: User
},
createddate:{ 
    type:Date,
    default:Date.now()
},
currentweight:{
   type:Number
},
goalweight:{
   type:Number
},
deleted:{
    type:String,enum:['yes','no']
}
});

module.exports = mongoose.model('Weight', weightSchema);