const mongoose = require('mongoose');
const User = require('../../signup/model/signup.model')

const activitySchema = mongoose.Schema({
user_id:{
    type:mongoose.ObjectId,
    ref: User
},
createddate:{ 
    type:Date,
    default:Date.now()
},
bp:{
   type:Number
},
heartrate:{
   type:Number
},
stpes:{
   type:Number
},
deleted:{
    type:String,enum:['yes','no']
}
});

module.exports = mongoose.model('activity', activitySchema);