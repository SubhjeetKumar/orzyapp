const mongoose = require('mongoose');
const User = require('../../signup/model/signup.model')

const sleepSchema = mongoose.Schema({
user_id:{
    type:mongoose.ObjectId,
    ref: User
},
createddate:{ 
    type:Date,
    default:Date.now()
},
currentsleep:{
   type:Number
},
averagesleep:{
  type:Number
},
unit:{
    type:String,
    enum: ['hr', 'mm']   
},
deleted:{
    type: String, enum: ['yes', 'no']
}
});

module.exports = mongoose.model('Sleep', sleepSchema);