


const mongoose = require('mongoose');
const User = require('../../signup/model/signup.model')

const stepSchema = mongoose.Schema({
user_id:{
    type:mongoose.ObjectId,
    ref: User
},
createddate:{ 
    type:Date,
    default:Date.now()
},
currentstep:{
   type:String
},
goalstep:{
    type:String
 },
distance:{
    type:String
 },
calorie:{
    type:String
 },
deleted:{
    type: String, enum: ['yes', 'no'],
    default:'no'
}
});

module.exports = mongoose.model('steps', stepSchema);