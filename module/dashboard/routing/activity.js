const express = require('express');
var request = require('request');
const router = new express.Router();
const path = require("path");
var mongoose = require('mongoose')
const key = require("../../../config/db_config");

const activityModel = require('../model/activity.model');


router.get('/v1/activitytest', (req, res) => {
    res.send('activity screen routes working');
});


//weight screen apis:::::
router.post('/v1/addactivity', (req, res) => {
    const activity = new activityModel(req.body);
    activity.save((err, data) => {
        if (data) {
            res.json({ statusCode: 200, message: "Success", data: data })
        }
        else {
            res.json({ statusCode: 400, message: "Not created" })
        }
    })

})
// get User goal::::::::
router.get('/v1/getuseractivity/:id',(req,res)=>{
    const user_id = req.params.id;
    activityModel.find({user_id},(err,data)=>{
        if(data){
            res.json({
                statusCode:200,
                userweight: data
            })
        }else{
            return res.json({statusCode:400, error:"Error"})
        }
    })
})
//update User goal weight:::::
router.put('/v1/updateusergoal/:id',
    (req, res) => {
        const user_id = req.params.id;

        activityModel.findOneAndUpdate({ user_id },
            req.body,
            { new: true },
            (err, userweight) => {
                if (err) {
                    res.json({ statusCode: 400, error: "Unable to Update user goal " });
                }
                res.json({ statusCode: 200, message: "User goal Updated Successfully" });
            });
    });




module.exports = router;