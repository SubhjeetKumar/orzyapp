const express = require('express');
var request = require('request');
const router = new express.Router();
const path = require("path");
var mongoose = require('mongoose')
const key = require("../../../config/db_config");

const weightModel = require('../model/weight.model');


router.get('/v1/weighttest',(req, res) => {
    res.send('weight screen routes working');
   });


//weight screen apis:::::
router.post('/v1/addweight', (req,res)=>{
    const Weight = new weightModel(req.body);  
    Weight.save((err,weightdata)=>{
        if(weightdata){
            res.json({statusCode:200,message:"Success",data:weightdata})
        }
        else{
            res.json({statusCode:400,message:"Not created"})
        }
    })
    
})
//get User weight::::::::
router.get('/v1/getuserweight',(req,res)=>{
    weightModel.find((err,weightusers)=>{
        if(weightusers){
            res.json({
                statusCode:200,
                userweight: weightusers
            })
        }else{
            return res.json({statusCode:400, error:"Error"})
        }
    })
})
//update User goal weight:::::
router.put('/v1/updateuserweight/:id',
(req,res)=>{
    const user_id = req.params.id;

    weightModel.findOneAndUpdate({ user_id },
      req.body,
      { new: true },
      (err, userweight) => {
      if (err) { 
        res.json({statusCode:400,error:"Unable to Update user goal Weight"});
      }
      res.json({statusCode:200,message:"User goal Weight Updated Successfully"});
    });
  });


  

module.exports = router;