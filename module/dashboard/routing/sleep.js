const express = require('express');
var request = require('request');
const router = new express.Router();
const path = require("path");
var mongoose = require('mongoose')
const key = require("../../../config/db_config");

const sleepModel = require('../model/sleep.model');


router.get('/v1/sleeptest',(req, res) => {
    res.send('sleep screen routes working');
   });


//Sleep Add Apis:::::
router.post('/v1/addsleep', (req,res)=>{
    const Sleep = new sleepModel(req.body);
    Sleep.save((err,sleepdata)=>{
        if(sleepdata){
            res.json({statusCode:200,message:"Successfully Added",data:sleepdata})
        }
        else{
            res.json({statusCode:400,message:"Not created"})
        }
    })
    
})
//get User Sleep::::
router.get('/v1/getusersleep',(req,res)=>{
    sleepModel.find((err,speelexercise)=>{
        if(speelexercise){
            res.json({
                statusCode:200,
                exercise: speelexercise
            })
        }else{
            return res.json({statusCode:400, error:"Error"})
        }
    })
})
//update user sleep:::::
router.put('/v1/updatesleep/:id',
(req,res)=>{
    const _id = req.params.id;
    sleepModel.findOneAndUpdate({ _id },
      req.body,
      { new: true },
      (err, speelexercise) => {
      if (err) { 
        res.json({statusCode:400,error:"Unable to Update Sleep"});
      }
      res.json({statusCode:200,message:"Sleep Updated Successfully"});
    });

  });
module.exports = router;