

const express = require('express');
var request = require('request');
const router = new express.Router();
const path = require("path");
var mongoose = require('mongoose')
const key = require("../../../config/db_config");

const bpModel = require('../model/bp.model');

router.get('/v1/bptest',(req, res) => {
    res.send('Bp screen routes working');
   });


//Bp Add Apis:::::
router.post('/v1/adduserbp', (req,res)=>{
    const bpdata = req.body;
    console.log("Data Recieved :::  ",bpdata);
    if(!bpdata.user_id){   
        res.json({statusCode:400,message:"user_id is mandatory"});
    }
    if(!bpdata.currentBP_DBP){
        res.json({statusCode:400,message:"currentBP_DBP is mandatory"});
    }
    if(!bpdata.currentBP_SBP){
        res.json({statusCode:400,message:"currentBP_SBP is mandatory"});
    }
    userbpdata={
        user_id:bpdata.user_id,
        currentBP_DBP:bpdata.currentBP_DBP,
        currentBP_SBP:bpdata.currentBP_DBP
    }
    const Bp = new bpModel(userbpdata);
    Bp.save((err,bpdata)=>{
        if(bpdata){
            res.json({statusCode:200,message:"Successfully Added",data:bpdata})
        }
        else{
            res.json({statusCode:400,message:"Not created"})
        }
    })
    
})
//get User Bp::::
router.get('/v1/getuserbp/:user_id',(req,res)=>{
    const user_id=req.params.user_id;
    bpModel.find({user_id},(err,userbp)=>{
        if(err){
            res.json({statusCode:400,message:"Ops erron occured",Error:err});
        }else{
            res.json({statusCode:200,message:"user bp fetched successfully",data:userbp})  
        }
    });
})
//update user sleep:::::
router.put('/v1/updateuserbp/:id',
(req,res)=>{
    const _id = req.params.id;
    bpModel.findOneAndUpdate({ _id },
      req.body,
      { new: true },
      (err, userbpdata) => {
      if (err) { 
        res.json({statusCode:400,error:"Oops some error occured",Error:err});
      }
      res.json({statusCode:200,message:"User BP Updated Successfully",data:userbpdata});
    });

  });
module.exports = router;