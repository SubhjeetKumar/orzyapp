const express = require('express');
var request = require('request');
const router = new express.Router();
const path = require("path");
var mongoose = require('mongoose')
const key = require("../../../config/db_config");

const stepModel = require('../model/step.model');


router.get('/v1/steptest',(req, res) => {
    res.send('step screen routes working');
   });


//Step Add Apis:::::
router.post('/v1/addstep', (req,res)=>{
    stepdata=req.body;
    if(!stepdata.user_id){
        res.json({statusCode:400,message:"user_id is mandatory"});
    }
    if(!stepdata.currentstep){
        res.json({statusCode:400,message:"currentstep is mandatory"});
    }
    if(!stepdata.goalstep){
        res.json({statusCode:400,message:"goalstep is mandatory"});
    }
    if(!stepdata.distance){
        res.json({statusCode:400,message:"goalstep is mandatory"});
    }
    if(!stepdata.calorie){
        res.json({statusCode:400,message:"calorie is mandatory"});
    }
    userstepsdata={
        user_id:stepdata.user_id,
        currentstep:stepdata.currentstep,
        goalstep:stepdata.goalstep,
        distance:stepdata.distance,
        calorie:stepdata.calorie
    }
    const Step = new stepModel(userstepsdata);
    Step.save((err,userstepsdata)=>{
        if(userstepsdata){
            res.json({statusCode:200,message:"Successfully Added",data:userstepsdata})
        }
        else{
            res.json({statusCode:400,message:"Not created"})
        }
    })
    
})
//get User Step::::
router.get('/v1/getuserstep',(req,res)=>{
    stepModel.find((err,stepdata)=>{
        if(stepdata){
            res.json({
                statusCode:200,
                message:"user step data fetched successful",
                data: stepdata
            })
        }else{
            return res.json({statusCode:400,message:"Oops some error occured", error:"Error"})
        }
    })
})

//get User Step by userid::::
router.get('/v1/getuserstep/:user_id',(req,res)=>{
    const user_id=req.params.user_id;
    stepModel.find({user_id},(err,stepdata)=>{
        if(stepdata){
            res.json({
                statusCode:200,
                message:"user step data fetched successful",
                data: stepdata
            })
        }else{
            return res.json({statusCode:400,message:"Oops some error occured", error:"Error"})
        }
    })
})

//update user step :::::
router.put('/v1/updatestep/:id',
(req,res)=>{
    const _id = req.params.id;
    stepModel.findOneAndUpdate({ _id },
      req.body,
      { new: true },
      (err, stepdata) => {
      if (err) { 
        res.json({statusCode:400,error:"Unable to Update Step"});
      }
      res.json({statusCode:200,message:"Step Updated Successfully",data:stepdata});
    });

  });
module.exports = router;