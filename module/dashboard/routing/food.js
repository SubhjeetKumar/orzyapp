const express = require('express');
var request = require('request');
const router = new express.Router();
const path = require("path");
var mongoose = require('mongoose')
const key = require("../../../config/db_config");

const foodModel = require('../model/food.model');


router.get('/v1/foodtest', (req, res) => {
    res.send('food routes working');
});


//food Add Apis:::::
router.post('/v1/addfood', (req, res) => {
    const food = new foodModel(req.body);
    food.save((err, data) => {
        if (data) {
            res.json({ statusCode: 200, message: "Successfully Added"})
        }
        else {
            res.json({ statusCode: 400, message: "Not created" })
        }
    })

})
//get User heartrate::::
// router.get('/v1/getheartrate', (req, res) => {
//     heartrateModel.find((err, data) => {
//         if (data) {
//             res.json({
//                 statusCode: 200,
//                 exercise: data
//             })
//         } else {
//             return res.json({ statusCode: 400, error: "Error" })
//         }
//     })
// })

//get food userwise:::::
router.get('/v1/getuserfood/:id',
    (req, res) => {
        const user_id = req.params.id;
        // console.log(user_id);
        foodModel.find({ user_id },
            (err, data) => {
                if (err) {
                    res.json({ statusCode: 400, error: "not find any data" });
                }
                res.json({ statusCode: 200, message: "Successfully", data: data });
            });

    });
module.exports = router;