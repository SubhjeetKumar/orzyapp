const express = require('express');
var request = require('request');
const router = new express.Router();
const path = require("path");
var mongoose = require('mongoose')
const key = require("../../../config/db_config");

const heartrateModel = require('../model/heartmonitor.model');


router.get('/v1/hearttest', (req, res) => {
    res.send('heartrate routes working');
});


//heartrate Add Apis:::::
router.post('/v1/addheartrate', (req, res) => {
    const heartrate = new heartrateModel(req.body);
    heartrate.save((err, data) => {
        if (data) {
            res.json({ statusCode: 200, message: "Successfully Added", data: data })
        }
        else {
            res.json({ statusCode: 400, message: "Not created" })
        }
    })

})
//get User heartrate::::
router.get('/v1/getheartrate', (req, res) => {
    heartrateModel.find((err, data) => {
        if (data) {
            res.json({
                statusCode: 200,
                exercise: data
            })
        } else {
            return res.json({ statusCode: 400, error: "Error" })
        }
    })
})
//update user heartrate userwise:::::
router.put('/v1/updateheartrate/:id',
    (req, res) => {
        const _id = req.params.id;
        heartrateModel.findOneAndUpdate({ _id },
            { currentheartrate: req.body.heartRate },
            { new: true },
            (err, data) => {
                if (err) {
                    res.json({ statusCode: 400, error: "Unable to Update heartrate" });
                }
                res.json({ statusCode: 200, message: "Heartrate Updated Successfully", data: data });
            });

    });

router.get('/v1/getheartratebyuser/:id', (req, res) => {
    const user_id = req.params.id;
    heartrateModel.find({ user_id }, (err, data) => {
        if (data) {
            res.json({
                statusCode: 200,
                exercise: data
            })
        } else {
            return res.json({ statusCode: 400, error: "Error" })
        }
    })
})

module.exports = router;