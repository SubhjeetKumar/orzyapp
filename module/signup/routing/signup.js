const express = require('express');
var request = require('request');
const bcrypt =require('bcryptjs')
const router = new express.Router();
const path = require("path");
var mongoose = require('mongoose')
const key = require("../../../config/db_config");
var otpGenerator = require('otp-generator')

const signupModel = require('../model/signup.model');
const { sign } = require('crypto');


const ym_id= "cb979083a7dbd56d0eac846b11129316";
const ym_secret= "034f4dc52fb309790e62e35e2799adb0";

//testing route
router.get('/user/create',(req, res) => {
 res.send('user routes working');
});

//otp send to mobile number
router.post('/v1/sendotp', (req,res)=>{
  const phone =req.body.phone; 
 
  signupModel.findOne({phone,otpverified:"No"}).then(userinfo=>{
    if(userinfo){  
      const otp1=otpGenerator.generate(4, { upperCase: false, specialChars: false,digits:true,alphabets:false });
      var date=new Date();
      date.setHours(date.getMinutes() + 10);
      var expiryTime = date.toISOString(); 
      var expiryTime =expiryTime
      const mobile=userinfo.phone;
      var dltContentId ='';
      // var text =`Your otp is ${otp1}. Please do not share it with anybody`;
      //     var url='https://api.flash49.com/fe/api/v1/send?username=sanjeevb.trans&password=kYfrQ&unicode=false&from=SHIVTL&to='+mobile +'&text=%27'+text +'%27&templateId=67454162';
      //     var options = {
      //       url : url,
      //       method : 'GET',        
      //       json: true
      //     };
      //     request(options, function (err,data) {
      //       if(err){
      //          res.status(400).json({error:"Error arises"}) 
      //         }
      //         else{
      //           res.status(200).json({Message:"OTP Sent successfully",id:userinfo._id});   
      //         }
      //     })
      var text = `Dear Partner , Your ArthPe verification code is ${otp1}`
        dltContentId = '1707161674253246402';
          
      var url = `https://wallet.arthpe.com/AllianceWallet-2.1.1.RELEASE/v1/allianceapi/sendsms/many?dltContentId=${dltContentId}`;
      var data = {
        "mobileNumbers": [{
            "mobileNumber": mobile
        }],
        "text": text
    }
    var options = {
        url: url,
        method: 'POST',
        body: data,
        headers: {
            'content-type': 'application/json',
            'Ym_Id': ym_id,
            'Ym_Secret': ym_secret,
        },
        json: true
    };
        request(options, function (err,data) {
        if(err){
           res.status(400).json({error:"Error arises"}) 
           console.log("Error occur while sending otp ::: ",err)
          }
          else{
            res.status(200).json({Message:"OTP Sent successfully",id:userinfo._id}); 
            console.log("Otp sent on :::",mobile)
              
          }
      })
     
        signupModel.findOneAndUpdate({phone},{otp:otp1,expiryTime:expiryTime},{new :true},(err,userinfo)=>{
          if(userinfo){
            console.log("success");
          }
        })  
      
    }
    else{
      const otp = otpGenerator.generate(4, { upperCase: false, specialChars: false,digits:true,alphabets:false });
      var date=new Date();
      date.setHours(date.getMinutes() + 10);
      var expiryTime = date.toISOString(); 
      var expiryTime =expiryTime
      // expiryDate=Math.round(new Date(expiryDate).getTime()/1000);
     
      const User = new signupModel({phone,expiryTime:expiryTime,otp:otp });
      User.save((err,user)=>{           
        if(user){ 
          //encrypt user id
          // const salt = bcrypt.genSalt(10);
          // user._id = bcrypt.hash(mongoose.Types.ObjectId(user._id), salt); 
         
          var mobile =user.phone;     
          // var text =`Your otp is ${user.otp}. Please do not share it with anybody`;
        
          // var url='https://api.flash49.com/fe/api/v1/send?username=sanjeevb.trans&password=kYfrQ&unicode=false&from=SHIVTL&to='+mobile +'&text=%27'+text +'%27&templateId=67454162';
          // var options = {
          //   url : url,
          //   method : 'GET',        
          //   json: true
          // };
          // request(options, function (err, resp, body) {
          //   if(err){
          //      res.status(400).json({error:"Error arises"}) 
          //     }
          //     else{
          //       res.status(200).json({message:"Otp sent successfully",id:user._id})   
          //     }
          // })
          // var text = `Dear Partner , Your ArthPe verification code is ${user.otp}`;
          var  text = `Dear Partner , Your ArthPe verification code is ${user.otp}`
        dltContentId = '1707161674253246402';
          console.log('else ::: ',mobile,user.otp)
          var url = `https://wallet.arthpe.com/AllianceWallet-2.1.1.RELEASE/v1/allianceapi/sendsms/many?dltContentId=${dltContentId}`;
          var data = {
            "mobileNumbers": [{
                "mobileNumber": mobile
            }],
            "text": text
        }
        var options = {
            url: url,
            method: 'POST',
            body: data,
            headers: {
                'content-type': 'application/json',
                'Ym_Id': ym_id,
                'Ym_Secret': ym_secret,
            },
            json: true
        };
            request(options, function (err,data) {
            if(err){
               res.status(400).json({error:"Error arises"}) 
               console.log("Error occur while sending message :::: ",err);
              }
              else{
                res.status(200).json({Message:"OTP Sent successfully",id:user._id}); 
                 console.log("(Else part) Otp sent on ::::  ",mobile);
              }
          })
          
        }
        else{ res.status(400).json({error:"No user found"})}     
      })
    }
  })
})

//verify otp
router.post('/v1/verify',(req,res)=>{
  const otp =req.body.otp;
  const id= req.body.id; 
   console.log(id,otp,"data>>>")
  signupModel.findOne({_id:id,otp:otp}).then(user=>{
     console.log(user,"datanew>>>")
    if(user){ 
      console.log("user ::::  ",user)
      res.json({statusCode:200,message:"You have been successfully verified",id:user._id})
     
      // var currentDate=new Date(); 
      // var expiryDate= new Date(user.expiryTime);
      // console.log(expiryTime,"data>>>>>")
      // currentDate= Math.round(new Date(currentDate).getTime()/1000);
      // expiryDate=Math.round(new Date(expiryDate).getTime()/1000);
      // console.log('expTime',expiryDate,currentDate);
     
      // if(expiryDate-currentDate<=0){
      //   res.json({message:"OTP expired"})
      // }
      // else{
      
      signupModel.findOneAndUpdate({_id:id},{otpverified:"Yes"} ,{new :true} ,(err,user) =>{
        if(err){
             console.log("Not updated", +err)
            } 
          else{
            console.log('success')  
        }
      })         
    }

  // }
  else
  {
    res.json({statusCode:400,message:"Wrong Otp"})
  }
 
    
}).catch(err=>console.log("error"))

});

//Resend Otp to Mobile
router.post('/v1/resend',(req,res)=>{
  const phone=req.body.phone;
  const otp = otpGenerator.generate(4, { upperCase: false, specialChars: false,digits:true,alphabets:false });
  //const User = new signupModel({otp:otp,phone:req.body.phone});
  //User.save
  console.log("req.body.phone ::: ",phone)
 // console.log("converted to object id :::  ",mongoose.ObjectId(_id))
  signupModel.findOneAndUpdate({phone:phone},{otp:otp},(err,user)=>{           
    if(user){ 
      console.log("resend otp user found")
      var mobile =user.phone; 
      
      // var otp =`Your otp is ${user.otp}. Please do not share it with anybody`;
      
      // var url='https://api.flash49.com/fe/api/v1/send?username=sanjeevb.trans&password=kYfrQ&unicode=false&from=SHIVTL&to='+mobile +'&text=%27'+otp +'%27&templateId=67454162';
      // var options = {
      //   url : url,
      //   method : 'GET',        
      //   json: true
      // };
      // request(options, function (err, data) {
      //   if(err){
      //      res.status(400).json({error:"Error arises"}) 
      //     }
      //     else{
      //       res.status(200).json({statusCode:200,message:"Otp sent successfully"})   
      //     }
      // })
      var text = `Dear Partner , Your ArthPe verification code is ${otp}`
        dltContentId = '1707161674253246402';
          
      var url = `https://wallet.arthpe.com/AllianceWallet-2.1.1.RELEASE/v1/allianceapi/sendsms/many?dltContentId=${dltContentId}`;
      var data = {
        "mobileNumbers": [{
            "mobileNumber": mobile
        }],
        "text": text
    }
    var options = {
        url: url,
        method: 'POST',
        body: data,
        headers: {
            'content-type': 'application/json',
            'Ym_Id': ym_id,
            'Ym_Secret': ym_secret,
        },
        json: true
    };
        request(options, function (err,data) {
        if(err){
           res.status(400).json({error:"Error arises"}) 
           console.log("Error occur while sending otp ::: ",err)
          }
          else{
            res.status(200).json({Message:"OTP Sent successfully on your phone"}); 
            console.log("Otp sent on :::",mobile)
              
          }
      })
     
       
    }
    else{ res.status(400).json({error:"No user found"})}    
  })

 
});

//questionier apis
router.post('/v1/profileverify/:id',(req,res)=>{
  const _id = req.params.id;
  console.log("id received :::  ",_id)
  console.log("data received :::  ",req.body)
  signupModel.findOne({_id},{isprofileverified:"No"}).then(user=>{
    if(user){
      const User = new signupModel(req.body);
      console.log("User on model ::::  ",User)
      User.save((err,profile)=>{
        if(profile){
          res.status(200).json({statusCode:200,message:"Profile Updated"})
          signupModel.findOneAndUpdate({_id:profile._id},{isprofileverified:"Yes"} ,{new :true} ,(err,user) =>{
                if(err){
                    console.log("Not updated", +err)
                    }
                else{
                    console.log('success')
                }
          }) 
        }
        else{
          res.json({statusCode:400,error:"Unable to create Profile"})
        }
      })       

    }
    else{
      res.json({statusCode:400,error:"User Profile already Verified"})
    }
  })
    
})

//signup user after otpauthentication
router.post('/v1/signup/:id',(req,res)=>{
  const _id = request.params.id;
  const userinfo = req.body;
  if(userinfo.firstName==undefined||userinfo.firstName==null||userinfo.firstName==""){
    res.json({statusCode:400,message:"firstName required cannot be empty null or undefined",id:userinfo._id})
  }
  if(userinfo.lastName==undefined||userinfo.lastName==null||userinfo.lastName==""){
    res.json({statusCode:400,message:"lastName required cannot be blank,null or undefined",id:userinfo._id})
  }
  if(!(userinfo.gender=='male'||userinfo.gender=='female'||userinfo.gender=='other')){
    res.json({statusCode:400,message:"gender must be from male or female or other",id:userinfo._id})
  }
  if(userinfo.measurementweight==undefined||userinfo.measurementweight==null||userinfo.measurementweight==""){
    res.json({statusCode:400,message:"measurementweight required cannot be blank,null or undefined",id:userinfo._id})
  }
  if(!(userinfo.measurementweightunit=='kgs'||userinfo.measurementweightunit=='lbs')){
    res.json({statusCode:400,message:"measurementweightunit must be in kgs or lbs",id:userinfo._id})
  }
  if(userinfo.measurementheight==undefined||userinfo.measurementheight==null||userinfo.measurementheight==""){
    res.json({statusCode:400,message:"measurementheight required cannot be blank,null or undefined",id:userinfo._id})
  }
  if(!(userinfo.measurementheightunit=='feet'||userinfo.measurementheightunit=='cms')){
    res.json({statusCode:400,message:"measurementheightunit must be in feet or cms",id:userinfo._id})
  }
  if(!(userinfo.goal=='weight loss'||userinfo.goal=='weight maintenance'||userinfo.goal=='muscle gain')){
    res.json({statusCode:400,message:"goal must be from weight loss or weight maintenance or muscle gain",id:userinfo._id})
  }
  if(!(userinfo.exercise=='no exercise'||userinfo.exercise=='1 day in a week'||userinfo.exercise=='1-3 days a week'||userinfo.exercise=='3-5 days a week')){
    res.json({statusCode:400,message:"goal must be from no exercise or 1 day in a week or 1-3 days a week or 3-5 days a week",id:userinfo._id})
  }

  
  userdetail = {
    firstName: userinfo.firstName,
    lastName: userinfo.lastName,
    gender: userinfo.gender,
    weight:{measurementscale:measurementweight,measurement:measurementweightunit},
    height:{measurementname:measurementheight,measurement:measurementheightunit},
    goal:userinfo.goal,
    exercise:userinfo.exercise,
    isdeleted:"No"
  }
  signupModel.findOneAndUpdate({_id},userdetail,{new: true},(err,result)=>{
          if(err){
            res.json({statusCode:400,message:"Some issues occur",Error:err,id:userinfo._id})
            console.log("Not updated", +err)
          } 
          else{
            res.json({statusCode:200,message:"Profile updated Successful",data:result})
            console.log('success')  
          }
  })
})

//User Login Apis:::::
router.post('/v1/userlogin', (req, res) =>{
  const phone = req.body.phone;
  const otp = req.body.otp;
  signupModel.findOne({ phone })
  .then(user => {
    if(!user) { 
      return res.status(404).json({ error: "User not found with this phone" });
    }
    bcrypt 
    .compare(otp, user.otp)
    .then(isCorrect => {
      if (isCorrect) {
        //use payload and create token for user
        const payload = {
          id: user._id,
          phone: user.phone          
        }; 

        jsonwt.sign(
          payload,
          key.secret,
          { expiresIn: '4h' }, 
          (err, token) => {
            
            res.json({
              success: true,
              statusCode:200,
              token:"Bearer "+ token,
              userid:user._id,
              message:"User Login Successfully"                                
           });
          }
        );
      } else {
        res.status(400).json({ statusCode:400, passworderror: "Password is not correct" });
      }
    })
    .catch(err => console.log(err));
})
.catch(err => console.log(err));
});
  
module.exports = router;