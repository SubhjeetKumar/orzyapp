const mongoose = require('mongoose');

const usersignupSchema = mongoose.Schema({
    phone: {
        type: String,
    },
    password: {
        type: String
    },
    confirmpassword: {
        type: String
    },
    usertype: {
        type: String
    },
    otp: {
        type: String
    },
    expiryTime: {
        type: String, trim: true
    },
    firstname: {
        type: String
    },
    lastname: {
        type: String
    },
    gender: {
        type: String, enum: ['male', 'female', 'other']
    },
    weight: {
        measurementscale: {
            type: String
        },
        measurement: {
            type: String,
            enum: ['kgs', 'lbs']
        }
    },
    height:
    {
        measurementname: {
            type: String
        },
        measurement: {
            type: String,
            enum: ['feet', 'cms']
        }
    },

    goal: {
        type: String,
        enum: ['weight loss', 'weight maintenance', 'muscle gain']
    },
    exercise: {
        type: String,
        enum: ['no exercise', '1 day in a week', '1-3 days a week', '3-5 days a week']
    },
    isdeleted: {
        type: String,
        default: "No"
    },
    isprofileverified: {
        type: String,
        default: "No"
    },
    otpverified: {
        type: String,
        default: "No"
    },
    createdDate: { type: Date, default: Date.now },
    updatedDate: { type: Date, default: null }

});

module.exports = mongoose.model('User', usersignupSchema);