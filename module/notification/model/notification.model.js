const mongoose = require('mongoose');

const notificationSchema = mongoose.Schema({
user_id:{
    type:String
},
device_type:{
    type:String
}
});

module.exports = mongoose.model('Notification', notificationSchema);