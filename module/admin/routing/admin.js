const express = require('express');
var request = require('request');
const bcrypt = require("bcryptjs");
const jsonwt = require("jsonwebtoken")
const router = new express.Router();
const path = require("path");
var mongoose = require('mongoose')
const key = require("../../../config/db_config");

const signupModel = require('../../signup/model/signup.model');

//Test admin route
router.get('/v1/admintest',(req, res) => {
    res.send('admin routes working');
   });


// user signup
router.post('/v1/adminsignup',(req,res)=>{

    signupModel.findOne({phone:req.body.phone}).then(user=>{
        if(user){
            res.json({statusCode:201,message:"User already register With this Phone number"})
        }
        else{
            const User = new signupModel(req.body);           
            const password =User.password;
            const confirmpassword =User.confirmpassword;            
            if (password === confirmpassword) {
                bcrypt.genSalt(10, (err, salt) => {
                  bcrypt.hash(password, salt, (err, hash) => {
                      if (err) throw err;                  
                      User.password = hash;   
                      User.confirmpassword = hash;
                      User.save(password); 
                  })
                })
                    User.save((err,data)=>{
                        if(err){
                            res.json({statusCode:400,message:"Not Created User"})
                        }
                        else{
                            res.json({statusCode:200,message:"User Created Successfully"})
                        }
                    })

                }       
        }
    
    })    
})

//User Login Apis:::::
router.post('/v1/login', (req, res) =>{
    const phone = req.body.phone;
    const password = req.body.password;
    signupModel.findOne({ phone })
    .then(user => {
      if(!user) { 
        return res.status(404).json({ error: "User not found with this phone" });
      }
      bcrypt 
      .compare(password, user.password)
      .then(isCorrect => {
        if (isCorrect) {
          //use payload and create token for user
          const payload = {
            id: user._id,
            phone: user.phone          
          }; 
  
          jsonwt.sign(
            payload,
            key.secret,
            { expiresIn: '4h' }, 
            (err, token) => {
              
              res.json({
                success: true,
                statusCode:200,
                token:"Bearer "+ token,
                userid:user._id,
                message:"User Login Successfully"                                
             });
            }
          );
        } else {
          res.status(400).json({ statusCode:400, passworderror: "Password is not correct" });
        }
      })
      .catch(err => console.log(err));
  })
  .catch(err => console.log(err));
  });
  
//logout user
router.get('/v1/logout', function(req, res){
  req.logout();
  res.status(200).json({message:"User logout successfully"})
 
});  
//forget User password::::
router.post('/v1/forgetpassword',(req, res)=>{
  signupModel.findOne({phone: req.body.phone})
  .then(user =>{
    if(!user){
      res.status(400).json({StatusCode:404,error:"No User Created With this Number"})
    }
    else{     
      // user.save();
      // var id =user._id;
      var mobile= user.phone;

      var text =`Reset Your Password`;
          var url='https://api.flash49.com/fe/api/v1/send?username=sanjeevb.trans&password=kYfrQ&unicode=false&from=SHIVTL&to='+mobile +'&text=%27'+text +'%27&templateId=67454162';
          var options = {
            url : url,
            method : 'GET',        
            json: true 
          };
          request(options, function (err,data) {
            if(err){
               res.status(400).json({error:"Error arises"}) 
              }
              else{
                res.status(200).json({Message:"successfully"});   
              }
          })
     
      }
  })
  .catch(err =>console.log("user not found", +err));
})

//Reset Ueser Password.....
router.post('/v1/reset/:id',(req,res)=>{
  const _id = req.params.id;
  
signupModel.findOne({ _id }) 
  .then(user =>{
    if(!user){ 
      res.status(400).json({error:"user Id not matched"});
    }else{  
      user.password = req.body.password,
      user.confirmpassword = req.body.confirmpassword
      const password =user.password;
      const confirmpassword =user.confirmpassword;

    if (password === confirmpassword) {
      bcrypt.genSalt(10, (err, salt) => {
        bcrypt.hash(password, salt, (err, hash) => {
            if (err) throw err;                  
            user.password = hash;   
            user.confirmpassword = hash;
            user.save(password); 
        })
      })
      if(user){
        res.status(200).json({ 
          succes:"true",
          message:"Password reset successfully"
        })
      }
      else{
        res.status(404).json({error:'password reset failed'});
      }
    
    }else{
      res.status(401).json({status:"false",message:"password not matched"})
    }
      
    }
    
  })
  .catch(err=>console.log("Not found any user id",+err));
})
  
  

module.exports = router;