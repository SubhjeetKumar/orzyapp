const express = require("express");
const mongoose = require('mongoose');
const { publicDecrypt } = require('crypto');
const passport = require("passport");
const bodyparser = require('body-parser');

const cors = require('cors');
const app = express();  


//mongoDb configruation
const db = require('./config/db_config').mongoURL

//attempt to connect Orzy Database
mongoose.connect(db,{ useNewUrlParser: true,useCreateIndex: true,useUnifiedTopology:true, useFindAndModify: false  })
    .then(() => console.log('MongoDB connected successfully'))
    .catch(err => console.log(err))


//Passport middleware
app.use(passport.initialize());  

//Config for JWT strategy
require("./strategies/jsonwtStrategy")(passport);

//middleware for body-parser
app.use(bodyparser.urlencoded({extended:false}));
app.use(bodyparser.json());


app.use(cors());
app.use(express.json());

//route importing

const signupRoutes = require('./module/signup/routing/signup');
const notifiationRoutes = require('./module/notification/routing/notification');
const weightRoutes = require('./module/dashboard/routing/weight');
const sleepRoutes = require('./module/dashboard/routing/sleep');
const adminRoutes = require('./module/admin/routing/admin');
const bpRoutes = require('./module/dashboard/routing/bp');
const stepRoutes = require('./module/dashboard/routing/step');
const heartrate = require('./module/dashboard/routing/heartmonitor');
const food = require('./module/dashboard/routing/food');
const activity = require('./module/dashboard/routing/activity');

app.use(signupRoutes);
app.use(notifiationRoutes);
app.use(weightRoutes);
app.use(sleepRoutes);
app.use(adminRoutes);
app.use(bpRoutes);
app.use(stepRoutes);
app.use(heartrate);
app.use(food);
app.use(activity);

//Test base route
// app.post('/test',(req,res)=>{
//   // res.send("app is working")
//   const data= req.body.data;
//   const cryptr = new Cryptr('myTotalySecretKey');
 
// const encryptedString = cryptr.encrypt(data);
// const decryptedString = cryptr.decrypt(encryptedString);
 
// console.log(encryptedString); 
// console.log(decryptedString); 

// })

const port = process.env.PORT || 3000;

app.listen(port, () => { 
    console.log(`server is running at port ${port}`);
})